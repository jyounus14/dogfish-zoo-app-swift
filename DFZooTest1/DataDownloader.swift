//
//  DataDownloader.swift
//  DFZooTest1
//
//  Created by jayy on 13/05/2015.
//  Copyright (c) 2015 jayy. All rights reserved.
//

import UIKit
import Alamofire

typealias doneCallback = (data:[Dictionary<String,String>]) -> (Void)

class DataDownloader: NSObject {
    
    static let instance = DataDownloader()
    
    func downloadJson(callback:doneCallback) {
        
        Alamofire.request(.GET, "http://www.dogfishdata.com/zoo/public/data.json")
            .responseJSON(options: nil) { (req:NSURLRequest, res:NSHTTPURLResponse?, json:AnyObject?, error:NSError?) -> Void in
                
                if let json : AnyObject = json {
                    //println("json: \(json)")
                    
                    let jsonDict = json as! Dictionary<String, AnyObject>
                    let animals = jsonDict["animals"] as! Dictionary<String, AnyObject>
                    
                    var index = 0
                    var animalsArray : [Dictionary<String,String>] = []
                    
                    while true {
                        index++
                        
                        var item : AnyObject? = animals["\(index)"] as AnyObject?
                        if let item : AnyObject = item {
                            let itemDict = item as! Dictionary<String, String>
                            animalsArray.append(itemDict)
                        } else {
                            break
                        }
                    }
                    
                    //println("constructed array: \(animalsArray)")
                    callback(data: animalsArray)
                }
                
                if let error = error {
                    println("error: \(error)")
                }
        }
    }
}
