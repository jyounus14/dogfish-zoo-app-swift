//
//  DetailsViewController.swift
//  DFZooTest1
//
//  Created by jayy on 13/05/2015.
//  Copyright (c) 2015 jayy. All rights reserved.
//

import UIKit

class DetailsViewController: UIViewController {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var animalLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var foodLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    
    var data : Dictionary<String, String>?
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        setup()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func setup() {
        if let data = data {
            
            self.title = data["name"]
            
            let ref = data["ref"]
            let url = NSURL(string: "http://www.dogfishdata.com/zoo/public/images/\(ref!).jpg")
            imageView.hnk_setImageFromURL(url!)
            
            nameLabel.text = data["name"]
            animalLabel.text = data["animal"]
            descriptionLabel.text = data["desc"]
            foodLabel.text = data["food"]
            
            let lat = data["lat"]
            let lon = data["lon"]
            
            locationLabel.text = "Lat: \(lat!)\nLon: \(lon!)"
        }
    }
}
