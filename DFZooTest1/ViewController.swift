//
//  ViewController.swift
//  DFZooTest1
//
//  Created by jayy on 13/05/2015.
//  Copyright (c) 2015 jayy. All rights reserved.
//

import UIKit
import Haneke

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    
    var data : [Dictionary<String,String>]?
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        downloadData()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("downloadData"), name: UIApplicationDidBecomeActiveNotification, object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func downloadData() {
        println("downloading data")
        
        DataDownloader.instance.downloadJson { (data) -> (Void) in
            self.data = data
            self.tableView.reloadData()
        }
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let data = data {
            return data.count
        }
        
        return 0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell1", forIndexPath: indexPath) as! AnimalCell
        
        if let data = data {
            let item = data[indexPath.row]
            
            let ref = item["ref"]
            let url = NSURL(string: "http://www.dogfishdata.com/zoo/public/images/\(ref!).jpg")
            
            cell.myImageView.hnk_setImageFromURL(url!)
            cell.nameLabel.text = item["name"]
            cell.animalLabel.text = item["animal"]
            cell.foodLabel.text = item["food"]
        }
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        println("row selected: \(indexPath.row)")
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        if let data = data {
            let vc = self.storyboard?.instantiateViewControllerWithIdentifier("DetailsVC") as! DetailsViewController
            vc.data = data[indexPath.row]
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

